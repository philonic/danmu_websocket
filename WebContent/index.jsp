<%@ page contentType="text/html; charset=utf-8" language="java"  import="java.util.*"%>

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="shortcut icon" href="favicon.ico"/>
	<link rel="bookmark" href="favicon.ico"/>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title> 弹幕 聊天室</title>
    <link rel="stylesheet" type="text/css" href="static/css/bootstrap.min.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="static/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="static/css/barrager.css">
    <link rel="stylesheet" type="text/css" href="static/pick-a-color/css/pick-a-color-1.2.3.min.css">
    <link type="text/css" rel="stylesheet" href="static/syntaxhighlighter/styles/shCoreDefault.css"/>
	<script type="text/javascript" src="static/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="static/js/tinycolor-0.9.15.min.js"></script>
	<script type="text/javascript" src="static/js/jquery.barrager.js"></script>
	<script type="text/javascript" src="static/syntaxhighlighter/scripts/shCore.js"></script>
	<script type="text/javascript" src="static/syntaxhighlighter/scripts/shBrushJScript.js"></script>
	<script type="text/javascript" src="static/syntaxhighlighter/scripts/shBrushPhp.js"></script>
	<script type="text/javascript" src="static/pick-a-color/js/pick-a-color-1.2.3.min.js"></script>
	<style type="text/css">

	.zp-leftbox{
	background-repeat: no-repeat;
	background-position: center 0 #000;
	background-color: #000;
	background-image: url(static/img/a.jpg);
	}

	.snowbg{width:1000px;height:600px;clear:both;margin:0 auto;position:relative;overflow:hidden;}
	.snow{position:absolute;top:0;color:#fff;}

</style>
	
</head>
<body class="bb-js" onload="scollMsg()">
<%
            String ip = request.getHeader("x-forwarded-for");
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                ip = request.getHeader("Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                ip = request.getHeader("WL-Proxy-Client-IP");
            }
            if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
                ip = request.getRemoteAddr();
            }
        %>
          <input id="IPAdr" name="IPAdr" value='<%=ip%>' type="hidden"> 
<!--左边大框-->
<div class="zp-leftbox">
    <div class="snowbg">
    <script type="text/javascript">
    $(function(){
      var d="<div class='snow'>❅<div>"
    			setInterval(function(){
    				var f=$(document).width();
    				var e=Math.random()*f-100;
    				var o=0.3+Math.random();
    				var fon=10+Math.random()*30;
    				var l = e - 100 + 200 * Math.random();
    				var k=2000 + 5000 * Math.random();
    				$(d).clone().appendTo(".snowbg").css({
    					left:e+"px",
    					opacity:o,
    					"font-size":fon,
    				}).animate({
    				  top:"400px",
    					left:l+"px",
    					opacity:0.1,
    				},k,"linear",function(){$(this).remove()})
    			},200)
    })
    </script>
</div>
</div>
<!--右边大框，评论-->
<div class="zp-rightbox">
    <div class="zp-item" id="msg-item"  style="OVERFLOW-Y:auto;OVERFLOW-X:hidden;">
	<script type="text/javascript">
	 $.getJSON("getAllMsg",function(data){   
	        //通过循环取出data里面的值         
	        $.each(data,function(i,value){  
	        	var $span = $("<span><u>"+ data[i].msg +"</u><em>"+data[i].sendtime+"</em>"
	                   +" <div class='sjx'></div>"
	                    +"</span>");
	        	var $parent = $("#msg-item"); // 获取<div>节点。<span>的父节点
	        	$parent.append($span); // 添加到<span>节点中，使之能在网页中显示
	        });      
	    });  	
	 
	 function scollMsg() { 
	 	var div = document.getElementById('msg-item');
 		div.scrollTop = div.scrollHeight;	
		 }
    </script>
    </div>
    <div class="container">
        <section class="bb-section">
            <div class="lead-top">

                <div class="text-center">

                    <p class="zp-cols">

                        <input type="text" id="text" class="enter">
                        <button  class="bb-trigger btn btn-primary btn-sm bb-light-blue" onclick="send();"> 发送</button>
                    </p>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="clearfix"></div>


<!-- JS dependencies -->
<script>
    var wsServer = 'ws://10.180.121.1:8080/danmu_websocket/websocket';
    //调用websocket对象建立连接：
    //参数：ws/wss(加密)：//ip:port （字符串）
    var websocket = new WebSocket(wsServer);
    //onopen监听连接打开
    websocket.onopen = function (evt) {
        //websocket.readyState 属性：
        /*
         CONNECTING    0    The connection is not yet open.
         OPEN    1    The connection is open and ready to communicate.
         CLOSING    2    The connection is in the process of closing.
         CLOSED    3    The connection is closed or couldn't be opened.
         */
        // msg.innerHTML = websocket.readyState;
    };

    $('.enter').keyup(function(e){
        if(e.keyCode == 13){
            send();
        }
    });

    function send(){
        var text = $('.enter').val();

        if(text.length==0){
            layer.msg('内容不得为空!');
            return false;
        }else{
            //向服务器发送数据
            text = $('#IPAdr').val()+":"+text;
            websocket.send(text);
            $('.enter').val('');
            $('.enter').focus();
        }
    }
    //监听连接关闭
    websocket.onclose = function (evt) {
        layer.msg("弹幕服务已被临时关闭!! ");
    };

    //onmessage 监听服务器数据推送
    websocket.onmessage = function (evt) {
        var jsondata = $.parseJSON(evt.data.toString());
        if(jsondata.status==0){
            alert("休息一下吧~~~");
        }else{
        	var $span = $("<span><u>"+ jsondata.info +"</u><em>"+jsondata.sendtime+"</em>"
	                   +" <div class='sjx'></div>"
	                    +"</span>");
	        	var $parent = $("#msg-item"); // 获取<div>节点。<span>的父节点
	        	$parent.append($span);
	        	var div = document.getElementById('msg-item');
	        	div.scrollTop = div.scrollHeight;
	        	
            $('.zp-leftbox').barrager(jsondata);
            
        }
        //console.log('Retrieved data from server: ' + evt.data);
    };
    //监听连接错误信息
    websocket.onerror = function (evt, e) {
        layer.msg('Error occured: ' + evt.data);
        // layer.msg("弹幕服务已被临时关闭!! ");
    };

</script>
</body>
</html>