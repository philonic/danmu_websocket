#danmu_websocket
环境要求：（现在开发用jre1.8.0_60+）
    jre1.7以上
    tomcat7.0以上

version：0.0.1
    1.在WebContent目录下新建lib文件夹
    2.在lib中加入fastjson-1.2.7.jar

version:0.0.2(2017年3月10日)
修改点：（使用postgreSQL存储弹幕消息和IP地址）
    1.新增druid数据库连接池utils（单例，稳定性还可以）
    2.新增MessageUtil包含message保存和查询最新150条的方法
    3.用户发送弹幕，会保存用户的IP地址和弹幕内容
    4.页面优化，加载或发弹幕后，弹幕历史为最新（目前还是不兼容IE浏览器）
    5.在lib中加入druid-1.0.18.jar
    6.在lib中加入postgresql-9.4.1212.jar



git命令：
    git add . 
    git  commit -m "changes log"
    git push -u origin master  