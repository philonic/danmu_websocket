package com.zhang.websocket;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zhang.common.ConnectionUtils;

public class MessageUtil {
	
	private MessageUtil(){}
	
	public static List<Map<String, Object>> showMessage(){
		List<Map<String, Object>> dataList = new ArrayList<Map<String,Object>>();  
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
	        try {
	            conn = ConnectionUtils.getConnection();

	            String sql = "select t.* from (select message msg,to_char(date_created,'yyyy-MM-dd HH24:mi:ss') sendTime from danmu order by sendTime desc limit 150) t order by t.sendTime";
	            pstm = conn.prepareStatement(sql);
	            rs = pstm.executeQuery();
	            ResultSetMetaData md = rs.getMetaData(); //获得结果集结构信息,元数据  
	            int columnCount = md.getColumnCount();   //获得列数   
	            while (rs.next()) {  
	                Map<String,Object> rowData = new HashMap<String,Object>();  
	                for (int i = 1; i <= columnCount; i++) {  
	                    rowData.put(md.getColumnName(i), rs.getObject(i));  
	                }  
	                dataList.add(rowData);  
	            }  

	            System.out.println("成功");
	        } catch (Exception e) {
	            System.out.println(String.format("失败，%s", e.getMessage()));
	        } finally {
	           closeResource(conn,pstm,rs);
	        }
	        return dataList;
	}

	public static void closeResource(Connection conn,
			PreparedStatement pstm, ResultSet rs) {
		 if (rs != null) {
             try {
            	 rs.close();
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }
         if (pstm != null) {
             try {
            	 pstm.close();
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }
         if (conn != null) {
             try {
            	 conn.close();
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }
		
	}

	public static void saveMessage(String message,String ipAddr,String userName) {
		Connection conn = null;
		PreparedStatement pstm = null;
		try {
            conn = ConnectionUtils.getConnection();

            
            String sql = "INSERT INTO public.danmu(message, ip_addr, user_name) VALUES (?, ?::inet, ?)";
            pstm = conn.prepareStatement(sql);
            pstm.setString(1,message);
            pstm.setObject(2, ipAddr);
            pstm.setString(3, userName);
            int count = pstm.executeUpdate();
            if (count > 0) {
                System.out.println("保存成功");
            }
        } catch (Exception e) {
            System.out.println(String.format("失败，%s", e.getMessage()));
        } finally {
           closeResource(conn,pstm,null);
        }
	}
}
