package com.zhang.common;

import java.sql.Connection;

import com.alibaba.druid.pool.DruidDataSource;
//alibaba数据库连接池，获取connection
public class ConnectionUtils {
	
    private static DruidDataSource dataSource = null;

    static {
        try {
            String driver = "org.postgresql.Driver";
            String url = "jdbc:postgresql://localhost:5432/postgres";
            String user = "postgres";
            String password = "900519";

            dataSource = new DruidDataSource();
            dataSource.setDriverClassName(driver);
            dataSource.setUrl(url);
            dataSource.setUsername(user);
            dataSource.setPassword(password);
            dataSource.setInitialSize(10);
            dataSource.setMinIdle(1);
            dataSource.setMaxActive(20);

            dataSource.setPoolPreparedStatements(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized Connection getConnection() {
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}