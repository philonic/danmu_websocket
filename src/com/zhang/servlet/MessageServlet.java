package com.zhang.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.zhang.websocket.MessageUtil;
@WebServlet("/getAllMsg")
public class MessageServlet extends HttpServlet {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 8482500010864154318L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	              throws ServletException, IOException { 
	          /**
	           * response.setContentType("application/Json;charset=UTF-8");目的是控制浏览器用UTF-8进行解码；
	           * 这样就不会出现中文乱码了
	           */
	          response.setHeader("content-type","application/Json;charset=UTF-8");
	          response.getWriter().println(JSONObject.toJSONString(MessageUtil.showMessage()));
	          response.getWriter().flush();
	      }

}
